import argparse
import os
import numpy as np
import clusterers
import cobs
from sklearn import metrics

parser = argparse.ArgumentParser()
parser.add_argument("dataset", help="the dataset to cluster")
parser.add_argument("output", help="file to which resulting clustering will be stored")
parser.add_argument("-c","--constraints", help="file containing constraints")
parser.add_argument("--silent", help="do not print any output when running",action="store_true")
parser.add_argument("-as","--all_sorted", help="sort all the algorithms from best to worst", action="store_true")
parser.add_argument("-ls","--labels_specified", help="should be set if last column of dataset contains cluster labels",action="store_true")
parser.add_argument("-nc","--num_constraints", help="the number of constraints to generate from the given cluster labels",type=int,default=50)
parser.add_argument("-a","--active", help="should be True for active constraint selection",action="store_true")
parser.add_argument("-s","--sample_size", help="size of sample from which constraints will be selected in active strategy",type=int,default=1000)
parser.add_argument("-sc","--store_clusterings", help="file to which generated clusterings can be stored")
parser.add_argument("-lc","--load_clusterings", help="file containing generated clusterings (from a previous run)")
parser.add_argument("-i","--interactive", help="if this flag is set, the user will be queried for pairwise relations through the command line",action="store_true")

args = parser.parse_args()

with open(args.dataset) as file:
    X = np.asarray([[float(digit) for digit in line.split(',')] for line in file])

labels = None
if args.labels_specified:
    labels = X[:,-1]
    X = X[:,:-1]

c = cobs.COBS()

## load or generate the set of clusterings to select from
if args.load_clusterings:
    c.load_clusterings_from_file(args.load_clusterings)
else:
    c.generate_clusterings(X, clusterers.generate_clusterers(X), args.store_clusterings)

## perform the clustering selection
if not args.active:
    # load/generate/ask ML and CL constraints
    if args.constraints:
        # load pre-defined constraints
        c.load_clusterings_from_file(args.constraints)
    elif args.interactive:
        # actually ask the user through the commandline
        c.ask_constraints_from_user(X.shape[0], args.num_constraints)
    elif args.labels_specified:
        # generate constraints from given cluster labels
        c.generate_constraints_from_labels(labels, args.num_constraints)
    else:
        print "You should either"
        print "\t (*) specify a file containing constraints (with -c or --constraints)"
        print "\t (*) or indicate that you will give them via the commandline (with -i or --interactive)"
        print "\t (*) or provide a dataset with known cluster labels (indicate with -ls or --labels_specified)"
        exit()

    clustering = c.run_random(all_sorted=args.all_sorted)
else:
    clustering = c.run_active(args.num_constraints,args.sample_size,range(X.shape[0]),labels,all_sorted=args.all_sorted)

## write the resulting clustering and the used ML and CL constraints
if not os.path.exists(args.output):
    os.makedirs(args.output)

if args.output.endswith('/'):
    args.output = args.output[:-1]

if args.all_sorted:
    with open(args.output + '/clusterings','w') as f:
        for method, setup, score in clustering:
            copy = dict(setup)
            copy['score'] = score
            copy['type'] = method.__name__
            f.write(str(copy) + "\n")
    if not args.silent:
        print "\n\n==========="
        print "COBS has finished, the result files are written to the following directory " + args.output
        print "The algorithms are written to the 'clusterings' file, sorted on score (relative between 0 and 1)"
else:
    with open(args.output + '/clustering','w') as f:
        for label in clustering[0]:
            f.write(str(label) + "\n")

    with open(args.output + '/ml','w') as f:
        for ml_c in c.ml:
            f.write(str(ml_c[0]) + "," + str(ml_c[1]) + "\n")

    with open(args.output + '/cl','w') as f:
        for cl_c in c.cl:
            f.write(str(cl_c[0]) + "," + str(cl_c[1]) + "\n")

    if not args.silent:
        print "\n\n==========="
        print "COBS has finished, the result files are written to the following directory " + args.output
        print "algorithm: " + str(clustering[1])
        print "parameter settings: " + ', '.join([str(param + "=" + str(clustering[2][param])) for param in clustering[2]])
        if args.labels_specified:
            print "ARI: " + str(metrics.adjusted_rand_score(clustering[0],labels))
