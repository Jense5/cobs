# COBS #

Code for Constraint-Based Selection (COBS) of clusterings. For more details on COBS, refer to

Toon Van Craenendonck and Hendrik Blockeel. "Constraint-based Clustering Selection", Machine Learning.

Contact me at toon.vancraenendonck_at_cs.kuleuven.be for any questions/comments.

# CONTENT #

* cobs.py: the actual COBS code

* clusterers.py: defines the search space (i.e. clustering algorithms and the hyperparameters that are varied)

* runner.py: command-line interface

* cv_runner.py: runs COBS in cross-validation and reports the mean test set ARI

# EXAMPLES #

Below are some examples on how to use the scripts, for more details execute `python runner.py -h`

* `python src/runner.py data/iris_with_labels.data output_dir -ls -nc 20`

    The first argument indicates the input file, which should be a comma-separated file containing only
    numeric values. The last column contains the class labels. The second argument indicates the output directory. For
    each run, the following files will be generated (1) output_dir/clustering (the generated clustering) (2) output_dir/ml (the used ML constraints) and
    (3) output_dir/cl (the used CL constraints).
    The  `-ls` flag indicates that the last column of the given dataset contains cluster labels.
    The `-nc 20` indicates that 20 constraints will be generated from the given labels, in this case at random.

* `python src/runner.py data/iris_with_labels.data iris_output -ls -nc 20 --all_sorted`

    The --all_sorted option will output a single file (output_dir/clusterings) with all the different algorithms and
    hyperparameters, sorted on the normalized score.

* `python src/runner.py data/iris_with_labels.data iris_output -ls -nc 20 --all_sorted --silent`

    Does the same as the previous command, but suppresses the output due to the --silent argument.

* `python src/runner.py data/iris_with_labels.data iris_output -ls -nc 20 -a`

    Same as before, but now active constraints selection is used to select more informative questions first.

* `python src/runner.py data/iris_without_labels.data iris_output -ls -c iris.constr`

    Same as the first one, but this time constraints are not generated from the labels, but rather given in a file, as
    indicated by `-c iris.constr`. Each line in this file should contain 3 numbers, separated by a comma.
    The first two numbers indicate the pair for which a constraint is specified, the third number should be 1 if the
    instances should be in the same cluster, and -1 if they should be in different clusters.

* `python src/runner.py data/iris_without_labels.data iris_output -ls -i -a`

    Same as the previous one, but in this case the user is queried for constraints (`-i` for interactive) and also most
    informative constraints are asked first (`-a` for active constraint selection).

It can be useful to do multiple selection runs with the same set of generated clusterings. For this reason, the
generated grid of clusterings can be stored and later on reloaded.

* `python src/runner.py data/iris_with_labels.data output_dir -ls -sc iris_clusterings`

    The generated clusterings are stored to a file named iris_clusterings

* `python src/runner.py data/iris_with_labels.data output_dir -ls -lc iris_clusterings`

    No clusterings will be generated, instead those generated previously and stored in iris_clusterings are used.

To perform cross-validation, use for example

* `python src/cv_runner.py data/iris_with_labels.data iris_cv_output -nf 10 -nc 50`

    The clusterings that are generated and the cross-validation folds are stored, such that they can be re-used in other
    experiments. For example, the next command performs cross-validation with the same clusterings and folds, but with
    60 constraints instead of 50.

* `python src/cv_runner.py data/iris_with_labels.data iris_cv_output -lf iris_cv_output/folds -lc iris_cv_output/clusterings -nf 10 -nc 60`
